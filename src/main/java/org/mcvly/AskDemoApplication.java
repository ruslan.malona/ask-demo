package org.mcvly;

import org.mcvly.data.Question;
import org.mcvly.service.QuestionRepository;
import org.mcvly.util.InitialDataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootApplication
@EnableScheduling
public class AskDemoApplication {
	private static final Logger logger = LoggerFactory.getLogger(AskDemoApplication.class);

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	CommandLineRunner initDb(QuestionRepository repository) {
		return strings -> {
			List<Question> questions = InitialDataUtil.readInitialData();
			questions.forEach(repository::save);
			logger.info("Initialized with " + questions.size() + " values");
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(AskDemoApplication.class, args);
	}
}
