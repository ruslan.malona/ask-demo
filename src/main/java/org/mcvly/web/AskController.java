package org.mcvly.web;

import org.mcvly.data.Question;
import org.mcvly.data.QuestionRejectionReason;
import org.mcvly.service.CountryCodeResolutionService;
import org.mcvly.service.QuestionStoreService;
import org.mcvly.service.QuestionValidationService;
import org.mcvly.service.RequestsStatisticService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class AskController {

    private static final Logger logger = LoggerFactory.getLogger(AskController.class);

    @Autowired
    private QuestionValidationService questionValidationService;
    @Autowired
    private QuestionStoreService questionStoreService;
    @Autowired
    private CountryCodeResolutionService countryService;
    @Autowired
    private RequestsStatisticService requestsStatisticService;

    @RequestMapping(value = "/question", method = RequestMethod.POST)
    public ResponseEntity<String> ask(@RequestBody @Valid Question question, HttpServletRequest request) {
        enrichWithCountryCode(question, getIpAddress(request));
        QuestionRejectionReason rejectionReason = questionValidationService.validate(question);
        if (rejectionReason == null) {
            questionStoreService.store(question);
            requestsStatisticService.addRequest(question.getOriginCountry());
            return new ResponseEntity<>("Question added", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(rejectionReason.getText(), HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @RequestMapping(value = "/question")
    public Iterable<Question> get(@RequestParam(value = "country", required = false) String country,
                                  @RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "limit", required = false) Integer size) {
        if (country != null && !country.isEmpty()) {
            return questionStoreService.getByCountryCode(country.toLowerCase());
        }
        if (page != null && size != null) {
            return questionStoreService.getAllPageable(page - 1, size);
        }
        return questionStoreService.getAll();
    }

    @RequestMapping(value = "/question/{questionId}")
    public ResponseEntity<?> byId(@PathVariable String questionId) {
        try {
            long l = Long.parseLong(questionId);
            Question res = questionStoreService.getById(l);
            if (res != null) {
                return new ResponseEntity<>(res, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            return new ResponseEntity<>("Cannot find question by id " + questionId + ": Wrong format", HttpStatus.BAD_REQUEST);
        }
    }

    private String getIpAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }
        logger.info(ipAddress);
        return ipAddress;
    }

    private void enrichWithCountryCode(Question question, String ipAddress) {
        String code = countryService.resolveCountryCodeByIp(ipAddress);
        question.setOriginCountry(code);
    }

}
