package org.mcvly.data;

public enum QuestionRejectionReason {
    BLACKLIST("Question contains blacklisted words"),
    MAX_FROM_COUNTRY("Too much connections from your country, try later");

    private final String text;

    QuestionRejectionReason(String s) {
        this.text = s;
    }

    public String getText() {
        return text;
    }
}
