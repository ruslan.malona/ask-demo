package org.mcvly.service;

public interface RequestsStatisticService {

    void addRequest(String countryCode);

    boolean exceeds(String countryCode, int timeFrameInMillis, long maxSessions);
}
