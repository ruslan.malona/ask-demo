package org.mcvly.service;

public interface BlackListService {
    boolean isBlackListed(String text);
}
