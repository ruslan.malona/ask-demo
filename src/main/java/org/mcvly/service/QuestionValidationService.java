package org.mcvly.service;

import org.mcvly.data.Question;
import org.mcvly.data.QuestionRejectionReason;

public interface QuestionValidationService {
    QuestionRejectionReason validate(Question question);
}
