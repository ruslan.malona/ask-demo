package org.mcvly.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Service
public class ConcurrentTreeRequestsStatisticService implements RequestsStatisticService {

    private static final Logger logger = LoggerFactory.getLogger(ConcurrentTreeRequestsStatisticService.class);

    private ConcurrentSkipListMap<Long, String> data = new ConcurrentSkipListMap<>();

    @Override
    public void addRequest(String countryCode) {
        data.put(System.nanoTime(), countryCode);
    }

    @Override
    public boolean exceeds(String countryCode, int timeFrameInMillis, long maxSessions) {
        long high = System.nanoTime();
        long low = high - timeFrameInMillis * 1_000_000L;
        ConcurrentNavigableMap<Long, String> subMap = data.subMap(low, high);
        long count = subMap.values().stream().filter(s -> s.equals(countryCode)).count();
        logger.info("Count is: " + count);
        return count >= maxSessions;
    }

    //test purpose only, very inefficient
    int dataSize() {
        return data.size();
    }

    @Scheduled(cron = "${statistics.clean.cron}")
    void reset() {
        data = new ConcurrentSkipListMap<>();
        logger.info("History has been reset");
    }
}
