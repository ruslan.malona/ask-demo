package org.mcvly.service;

import org.mcvly.data.Question;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends PagingAndSortingRepository<Question, Long> {
    List<Question> findByOriginCountry(String country);
    Question findById(Long id);
}
