package org.mcvly.service;

import org.mcvly.data.Question;

public interface QuestionStoreService {
    void store(Question question);
    Iterable<Question> getAll();
    Iterable<Question> getAllPageable(int pageNumber, int pageSize);
    Iterable<Question> getByCountryCode(String country);
    Question getById(Long questionId);
}
