package org.mcvly.service;

import org.mcvly.util.InitialDataUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

@Service
public class DictionaryBlackListService implements BlackListService {
    private static final Logger logger = LoggerFactory.getLogger(InitialDataUtil.class);

    private final Set<String> dictionary = new HashSet<>();

    @PostConstruct
    public void init() {
        try {
            URL resource = DictionaryBlackListService.class.getClassLoader().getResource("blacklist.txt");
            Stream<String> stream = Files.lines(Paths.get(resource.toURI()));
            stream.forEach(s -> dictionary.add(s.toLowerCase()));
            logger.info("read " + dictionary.size() + " items into black list");
        } catch (Exception e) {
            logger.error("Exception while reading initial data", e);
        }
    }

    @Override
    public boolean isBlackListed(String text) {
        if (text == null || text.isEmpty()) {
            return false;
        }
        String[] split = text.split("[^\\p{IsAlphabetic}]");
        for (String s : split) {
            if (dictionary.contains(s.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    int getDictionarySize() {
        return dictionary.size();
    }
}
