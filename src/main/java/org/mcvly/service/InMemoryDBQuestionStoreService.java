package org.mcvly.service;

import org.mcvly.data.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InMemoryDBQuestionStoreService implements QuestionStoreService {

    private QuestionRepository repository;

    @Autowired
    public InMemoryDBQuestionStoreService(QuestionRepository repository) {
        this.repository = repository;
    }

    @Override
    public void store(Question question) {
        repository.save(question);
    }

    @Override
    public Iterable<Question> getAll() {
        return repository.findAll();
    }

    @Override
    public Iterable<Question> getAllPageable(int pageNumber, int pageSize) {
        Pageable pageable = new PageRequest(pageNumber, pageSize);
        return repository.findAll(pageable);
    }

    @Override
    public Iterable<Question> getByCountryCode(String country) {
        return repository.findByOriginCountry(country);
    }

    @Override
    public Question getById(Long questionId) {
        return repository.findById(questionId);
    }
}
