package org.mcvly.service;

import org.mcvly.data.Question;
import org.mcvly.data.QuestionRejectionReason;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class QuestionValidationServiceImpl implements QuestionValidationService {

    private BlackListService blackListService;
    private RequestsStatisticService statisticService;

    @Value("${max.requests.from.country}")
    private Integer maxRequestFromCountry;

    @Value("${max.requests.timeframe.millis}")
    private Integer maxRequestsTimeFrame;

    @Autowired
    public QuestionValidationServiceImpl(BlackListService blackListService, RequestsStatisticService statisticService) {
        this.blackListService = blackListService;
        this.statisticService = statisticService;
    }

    @Override
    public QuestionRejectionReason validate(Question question) {
        if (blackListService.isBlackListed(question.getText())) {
            return QuestionRejectionReason.BLACKLIST;
        }
        if (statisticService.exceeds(question.getOriginCountry(), maxRequestsTimeFrame, maxRequestFromCountry)) {
            return QuestionRejectionReason.MAX_FROM_COUNTRY;
        }
        return null;
    }
}
