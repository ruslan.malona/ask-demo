package org.mcvly.service;

import org.mcvly.data.CountryInfo;
import org.mcvly.web.AskController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FreeGeoIPCountryCodeResolutionService implements CountryCodeResolutionService {

    private static final Logger logger = LoggerFactory.getLogger(AskController.class);

    private RestTemplate restTemplate;

    @Value("${default.country.code}")
    private String defaultCountryCode;

    @Autowired
    public FreeGeoIPCountryCodeResolutionService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public static final String GEO_IP_API = "http://freegeoip.net/json/";

    @Override
    public String resolveCountryCodeByIp(String ip) {
        if (ip != null && !ip.isEmpty()) {
            try {
                CountryInfo countryData = getCountryInfo(ip);
                if (countryData != null) {
                    String countryCode = countryData.getCountryCode();
                    if (countryCode != null && !countryCode.isEmpty()) {
                        return countryCode.toLowerCase();
                    }
                }
            } catch (Exception e) {
                logger.error("Cannot get country code for IP: " + ip, e);
            }
        }
        return defaultCountryCode;
    }

    CountryInfo getCountryInfo(String ip) {
        return restTemplate.getForObject(GEO_IP_API + ip, CountryInfo.class);
    }

    String getDefaultCountryCode() {
        return defaultCountryCode;
    }
}
