package org.mcvly.service;

public interface CountryCodeResolutionService {
    String resolveCountryCodeByIp(String ip);
}
