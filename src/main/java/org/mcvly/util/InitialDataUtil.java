package org.mcvly.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mcvly.data.Question;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

public class InitialDataUtil {
    private static final Logger logger = LoggerFactory.getLogger(InitialDataUtil.class);

    public static List<Question> readInitialData() {
        try {
            InputStream resourceAsStream = InitialDataUtil.class.getClassLoader().getResourceAsStream("initialData.json");
            if (resourceAsStream != null) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                return mapper.readValue(resourceAsStream, new TypeReference<List<Question>>() {});
            } else {
                logger.error("Cannot read file with initial data");
            }
        } catch (Exception e) {
            logger.error("Exception while reading initial data", e);
        }
        return Collections.emptyList();
    }

}
