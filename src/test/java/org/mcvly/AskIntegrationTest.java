package org.mcvly;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mcvly.data.Question;
import org.mcvly.data.QuestionRejectionReason;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.ObjectContent;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AskIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    private JacksonTester<List<Question>> json;

    @Before
    public void setup() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void testInitialDataLoaded() {
        List questions = this.restTemplate.getForObject("/question", List.class);
        assertThat(questions.size(), greaterThanOrEqualTo(5));
    }

    @Test
    public void testAddValidQuestion() {
        Question q = new Question("What is your age?");
        q.setOriginCountry("uk");
        this.restTemplate.postForEntity("/question", q, Void.class);
        List questions = this.restTemplate.getForObject("/question", List.class);
        assertEquals(6, questions.size());
    }

    @Test
    public void testWillNotAddFromBlackList() {
        List questions = this.restTemplate.getForObject("/question", List.class);
        int sizeBefore = questions.size();
        Question q = new Question("What is your,bla age?");
        q.setOriginCountry("uk");
        ResponseEntity<String> stringResponseEntity = this.restTemplate.postForEntity("/question", q, String.class);
        questions = this.restTemplate.getForObject("/question", List.class);
        assertEquals(sizeBefore, questions.size());
        assertEquals(HttpStatus.NOT_ACCEPTABLE, stringResponseEntity.getStatusCode());
        assertEquals(QuestionRejectionReason.BLACKLIST.getText(), stringResponseEntity.getBody());
    }

    @Test
    public void testQueryByCountry() throws IOException {
        String res = this.restTemplate.getForObject("/question?country=us", String.class);
        ObjectContent<List<Question>> parse = json.parse(res);
        assertThat(parse.getObject().size(), is(2));
        for (Question q : parse.getObject()) {
            assertThat(q.getOriginCountry(), equalTo("us"));
        }
    }

    @Test
    public void testGetById() throws IOException {
        String res = this.restTemplate.getForObject("/question/1", String.class);
        ObjectContent<List<Question>> parse = json.parse(res);
        assertThat(parse.getObject().size(), is(1));
        Question q = parse.getObject().get(0);
        assertEquals("us", q.getOriginCountry());
        assertEquals("Who is your favorite artist or painter?", q.getText());
    }
}
