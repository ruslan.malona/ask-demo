package org.mcvly.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mcvly.data.Question;
import org.mcvly.data.QuestionRejectionReason;
import org.mcvly.service.CountryCodeResolutionService;
import org.mcvly.service.QuestionStoreService;
import org.mcvly.service.QuestionValidationService;
import static org.mockito.Mockito.*;

import org.mcvly.service.RequestsStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = {AskControllerTest.TestContext.class})
public class AskControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private QuestionStoreService questionStoreService;

    @Autowired
    private QuestionValidationService questionValidationService;

    @Autowired
    private RequestsStatisticService requestsStatisticService;

    @Autowired
    private CountryCodeResolutionService countryCodeResolutionService;

    @Before
    public void setup() {
        reset(requestsStatisticService);
        reset(questionValidationService);
        reset(questionStoreService);
        reset(countryCodeResolutionService);
    }

    @Test
    public void emptyPostShouldReturnError() throws Exception {

        this.mockMvc
                .perform(post("/question"))
                .andDo(print()).andExpect(status().isBadRequest());

        this.mockMvc
                .perform(post("/question")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                .andDo(print()).andExpect(status().isBadRequest());
    }

    @Test
    public void questionWithoutContentShouldReturnError() throws Exception {

        this.mockMvc
                .perform(post("/question")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"content\" : \"\"}"))
                .andDo(print()).andExpect(status().isBadRequest());
        verify(questionValidationService, never()).validate(any());
        verify(questionStoreService, never()).store(any());
        verify(requestsStatisticService, never()).addRequest(any());
    }

    @Test
    public void validQuestionShouldReturnOK() throws Exception {
        Question question = new Question("abc");
        question.setOriginCountry("lv");
        when(questionValidationService.validate(eq(question))).thenReturn(null);
        when(countryCodeResolutionService.resolveCountryCodeByIp(anyString())).thenReturn("lv");
        this.mockMvc
                .perform(post("/question")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"text\" : \"abc\"}"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().string("Question added"));
        verify(questionValidationService, times(1)).validate(eq(question));
        verify(questionStoreService, times(1)).store(eq(question));
        verify(requestsStatisticService, times(1)).addRequest(eq("lv"));
        verify(countryCodeResolutionService, times(1)).resolveCountryCodeByIp(anyString());
    }

    @Test
    public void invalidQuestionRejected() throws Exception {
        Question question = new Question("inappropriate");
//        question.setOriginCountry("lv");
        when(questionValidationService.validate(any())).thenReturn(QuestionRejectionReason.BLACKLIST);
        this.mockMvc
                .perform(post("/question")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"text\" : \"inappropriate\"}"))
                .andDo(print())
                .andExpect(status().isNotAcceptable())
                .andExpect(content().string(QuestionRejectionReason.BLACKLIST.getText()));
        verify(questionValidationService, times(1)).validate(eq(question));
        verify(questionStoreService, times(0)).store(eq(question));
        verify(requestsStatisticService, never()).addRequest(any());
    }

    @Test
    public void testGetAllQuestions() throws Exception {
        this.mockMvc
                .perform(get("/question"))
                .andDo(print());
        verify(questionStoreService).getAll();
    }

    @Test
    public void testGetPageable() throws Exception {
        this.mockMvc
                .perform(get("/question?page=1&limit=1"))
                .andDo(print());
        verify(questionStoreService, times(0)).getAll();
        verify(questionStoreService).getAllPageable(eq(0), eq(1));
    }

    @Test
    public void testPageableBothParametersMustBeSupplied() throws Exception {
        this.mockMvc
                .perform(get("/question?page=1"))
                .andDo(print());
        verify(questionStoreService, times(1)).getAll();
        verify(questionStoreService, times(0)).getAllPageable(anyInt(), anyInt());

        reset(questionStoreService);

        this.mockMvc
                .perform(get("/question?limit=1"))
                .andDo(print());
        verify(questionStoreService, times(1)).getAll();
        verify(questionStoreService, times(0)).getAllPageable(anyInt(), anyInt());
    }

    @Test
    public void testGetByCountry() throws Exception {
        this.mockMvc
                .perform(get("/question?country=us"))
                .andDo(print());
        verify(questionStoreService).getByCountryCode(eq("us"));
    }

    @Test
    public void testGetById() throws Exception {
        this.mockMvc
                .perform(get("/question/1"))
                .andDo(print());
        verify(questionStoreService).getById(eq(1L));
    }

    @Configuration
    @EnableWebMvc
    @ComponentScan("org.mcvly.web")
    public static class TestContext {

        @Bean
        public QuestionValidationService questionValidationService() {
            return mock(QuestionValidationService.class);
        }

        @Bean
        public QuestionStoreService testQuestionStoreService() {
            return mock(QuestionStoreService.class);
        }

        @Bean
        public CountryCodeResolutionService countryCodeResolutionService() {
            return mock(CountryCodeResolutionService.class);
        }

        @Bean
        public RequestsStatisticService requestsStatisticService() {
            return mock(RequestsStatisticService.class);
        }

    }
}