package org.mcvly.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mcvly.data.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InMemoryDBQuestionStoreServiceIntegrationTest {

    @Autowired
    private QuestionStoreService service;

    @Test
    public void testDataLoaded() {
        List<Question> all = (List<Question>) service.getAll();
        assertNotNull(all);
        assertEquals(5, all.size());
    }

    @Test
    public void testInsert() {
        Question newQuestion = new Question("blabla");
        newQuestion.setOriginCountry("uk");
        service.store(newQuestion);
        List<Question> all = (List<Question>) service.getAll();
        assertEquals(6, all.size());
    }

    @Test
    public void testByCountry() {
        List<Question> us = (List<Question>) service.getByCountryCode("us");
        assertEquals(2, us.size());
    }

    @Test
    public void testById() {
        Question byId = service.getById(1L);
        assertNotNull(byId);
        assertEquals("us", byId.getOriginCountry());
    }

}