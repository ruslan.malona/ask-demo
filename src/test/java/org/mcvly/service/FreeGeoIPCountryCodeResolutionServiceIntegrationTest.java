package org.mcvly.service;

import org.junit.Test;
import org.mcvly.data.CountryInfo;
import org.mcvly.service.FreeGeoIPCountryCodeResolutionService;
import org.springframework.boot.test.TestRestTemplate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FreeGeoIPCountryCodeResolutionServiceIntegrationTest {

    private TestRestTemplate restTemplate = new TestRestTemplate();

    private FreeGeoIPCountryCodeResolutionService service = new FreeGeoIPCountryCodeResolutionService(restTemplate);

    @Test
    public void resolveCountryCodeByIp() throws Exception {
        String ip = "109.197.218.46";
        CountryInfo countryInfo = service.getCountryInfo(ip);
        System.out.println(countryInfo);
        assertNotNull(countryInfo);
        assertEquals(ip, countryInfo.getIp());
        assertEquals("UA", countryInfo.getCountryCode());
    }

    @Test
    public void testWrongIp() {
        String ip = "333.333.333.333";
        String code = service.resolveCountryCodeByIp(ip);
        assertEquals(service.getDefaultCountryCode(), code);
    }

}