package org.mcvly.service;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;

public class ConcurrentTreeRequestsStatisticServiceTest {

    private ConcurrentTreeRequestsStatisticService service = new ConcurrentTreeRequestsStatisticService();

    @Before
    public void setup() {
        service.reset();
    }

    @Test
    public void testRange() throws InterruptedException {
        service.addRequest("ua");
        service.addRequest("ua");
        Thread.sleep(100);
        service.addRequest("ua");
        service.addRequest("ua");
        assertFalse(service.exceeds("ua", 100, 3));
    }

    @Test
    public void testMultiThreadedInsert() throws InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        List<Future<String>> futures = pool.invokeAll(Arrays.asList(new Task("ua", 1000), new Task("lv", 500)));
        List<String> collect = futures.stream().map(f -> {
            try {
                String s = f.get(5, TimeUnit.SECONDS);
                System.out.println("executed " + s);
                return s;
            } catch (Exception e) {
                System.err.println(e.getMessage());
                return null;
            }
        }).collect(toList());
        assertEquals(2, collect.size());
        assertEquals(1500, service.dataSize());
        assertTrue(service.exceeds("ua", 5000, 1000));
        assertFalse(service.exceeds("lv", 5000, 501));
        pool.shutdown();
    }

    @Test
    @Ignore("should be run manually with heap monitoring")
    public void testStability() throws InterruptedException {
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                service.reset();
            }
        }).start();
        while (true) {
            for (int i = 0; i < 100; i++) {
                service.addRequest("ua");
            }
            Thread.sleep(5);
            service.exceeds("ua", 1000, 10);
            System.out.println(service.dataSize());
        }
    }

    private class Task implements Callable<String> {
        private String code;
        private int count;

        Task(String code, int count) {
            this.code = code;
            this.count = count;
        }

        @Override
        public String call() throws Exception {
            for (int i = 0; i < count; i++) {
                service.addRequest(code);
            }
            return code;
        }
    }

}