package org.mcvly.service;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class DictionaryBlackListServiceTest {

    private static DictionaryBlackListService dictionaryBlackListService = new DictionaryBlackListService();

    @BeforeClass
    public static void init() {
        dictionaryBlackListService.init();
    }

    @Test
    public void testDictionaryLoaded() {
        assertEquals(2, dictionaryBlackListService.getDictionarySize());
    }

    @Test
    public void testNonBlackListText() {
        String text = "What is your name?";
        assertFalse(dictionaryBlackListService.isBlackListed(text));
    }

    @Test
    public void testBlackListSeparatedBySpace() {
        String text = "What is your bla name?";
        assertTrue(dictionaryBlackListService.isBlackListed(text));
    }

    @Test
    public void testBlackListNonEnglish() {
        String text = "Как тебя зовут, йопт?";
        assertTrue(dictionaryBlackListService.isBlackListed(text));
    }

    @Test
    public void testBlackListNotSeparated() {
        String text = "What is your bla?";
        assertTrue(dictionaryBlackListService.isBlackListed(text));
        String text2 = "What is your bla, name";
        assertTrue(dictionaryBlackListService.isBlackListed(text2));
        String text3 = "What is your bla! name";
        assertTrue(dictionaryBlackListService.isBlackListed(text3));
        String text4 = "What is your !bla1 name";
        assertTrue(dictionaryBlackListService.isBlackListed(text4));
    }
}