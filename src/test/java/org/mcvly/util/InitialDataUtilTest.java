package org.mcvly.util;

import org.junit.Test;
import org.mcvly.data.Question;

import java.util.List;

import static org.junit.Assert.*;

public class InitialDataUtilTest {

    @Test
    public void testDataLoaded() {
        List<Question> questions = InitialDataUtil.readInitialData();
        assertNotNull(questions);
        assertEquals(5, questions.size());
        assertEquals("us", questions.get(0).getOriginCountry());
    }

}