# Ask-Demo application
This is the application which simulates basic QA-service

## Public API
- `POST /question` add new question 
- `GET /question` return all questions (pageable)
- `GET /question?country=<code>` return questions from specified country
- `GET /question/<id>` return question by id

## Add new question
To add new question send **POST** request to` /question` endpoint. It should have header `Content-Type: application/json` and body in JSON format: 
```
{
  "text": "What is your name?"
}
```
If adding was successful client receives `Status Code ` **201** and `Response Body ` **Question added**

There are some conditions when the question can be rejected:
- it contains words listed in Black List Dictionary
- if over **N** request / per some time frame are being sent from same country

In that case response would be:  `Status Code ` **406** and `Response Body ` **Question contains blacklisted words** or **Too much connections from your country, try later** respectively

## Retrieve added questions
**Get all questions**

`GET /question` - will return all added questions without limit. Example response:
```
[
  {
    "id": 1,
    "text": "Who is your favorite artist or painter?",
    "originCountry": "us"
  },
  {
    "id": 2,
    "text": "Чем тебе запомнилось это лето?",
    "originCountry": "ru"
  }
]
```
**Get all questions by pages**

`GET /question?page=1&limit=3` - will return pageable response limiting results. Example response:
```
{
  "content": [
    {
      "id": 1,
      "text": "Who is your favorite artist or painter?",
      "originCountry": "us"
    },
    {
      "id": 2,
      "text": "Чем тебе запомнилось это лето?",
      "originCountry": "ru"
    },
    {
      "id": 3,
      "text": "Яким, на твій погляд, буде майбутнє?",
      "originCountry": "ua"
    }
  ],
  "last": false,
  "totalElements": 5,
  "totalPages": 2,
  "sort": null,
  "first": true,
  "numberOfElements": 3,
  "size": 3,
  "number": 0
}
```
**Get questions by country**

`GET /question?country=us` - will return only questions where `originCountry` equals given code. Example response:
```
[
  {
    "id": 1,
    "text": "Who is your favorite artist or painter?",
    "originCountry": "us"
  },
  {
    "id": 5,
    "text": "Would you follow the white rabbit?",
    "originCountry": "us"
  }
]
```
**Get questions by id**

`GET /question/1` - will return single question if found, otherwise Error **404**. Example response:
```
{
  "id": 1,
  "text": "Who is your favorite artist or painter?",
  "originCountry": "us"
}
```

## Installation and execution details
### Prerequisites
Technical requirements: JDK 8
### Start server
To start server execute following command in directory with project:
`./mvnw spring-boot:run`
### Start using:
If no errors appeared in console, server is available at `http://localhost:8080`. If this port is busy, you can change it in `src/main/resources/application.properties`
### Configuration:
- **black list** file is located at `src/main/resources/blacklist.txt` It's content is loaded in memory on server startup
- **initial data** is at `src/main/resources/initialData.json` This file contains questions that are added on server during startup. It's basically for testing purposes.
- **application.properties** is at `src/main/resources/applications.properties`. There you can change in-memory DB url, statistics clean-up interval (see below) and server's port

## Implementation Details
### Tech Stack
Server is written in Java 8 using Spring framework (Components: Boot, Core, MVC, Data, Test). Data is persisted into in-memory database H2. Library for JSON serialization: Jackson. Build system: Maven (already included in project dir) Web container: embedded Jetty
### Testing
There are unit and integration tests. Unit tests are run during standard Maven phase test: `./mvnw clean test`

**Unit tests description:**
- `AskControllerTest`: Tests main controller's methods. Mocks all services used by controller and checks their interaction using Mockito.
- `InitialDataUtilTest`: Tests that initial data from file `initialData.json` is loaded in memory
- `DictionaryBlackListServiceTest`: Tests black list service
- `ConcurrentTreeRequestsStatisticServiceTest`: Tests RequestsStatisticsService (see below)

**Integration tests description:**
These tests are not run automatically during build because they require external resources or whole server been started up
- `AskIntegrationTest`: Main integration test. Checks all parts of system. It is performing whole cycle testing: on live server it sends get and post request to controller, validates responses and that data is inserted in db. Also checks that Black List service is working properly. The only thing it doesn't test is RequestsStatisticsService, it's being tested in separate test
- `InMemoryDBQuestionStoreServiceIntegrationTest`: Checks that data is loaded, inserted and queried by InMemoryDBQuestionStoreService
- `FreeGeoIPCountryCodeResolutionServiceIntegrationTest`: Checks that we can resolve country code by IP using FreeGeoIP Service
### Services Implementation
**Requests Statistic Service**

Essential problem it tries to solve is to limit number of questions coming from a country in a given timeframe
We're using data structure ConcurrentSkipListMap to keep track of all requests and countries from which they're initiated
Keys are `System.nanoTime()` - this should be unique characteristic even for large amount of requests. 
For deciding if request exceeds some limit from country we're using method `subMap` which return part of map defined by high and low keys. They are current nano time and current time minus specified limit respectively. 
Then wew filter it by request's country code and then compare result size with maximum allowed connections.
Requests are added to this structure on successful save into DB.
Using ConcurrentSkipListMap allowed us to be able to work effectively with this structure concurrently
There's however some issue with this approach. With time this structure grows and consumes more and more memory. 
To overcome this issue we're running a scheduled job which clears whole statistics to free memory.
Service configuration in `application.properties`:
- `statistics.clean.cron` Cron expression which defines how often to start statistics clean-up
- `max.requests.from.country` How many simultaneous request from same country are allowed within timeframe
- `max.requests.timeframe.millis` Defines timeframe in milliseconds within which we want to limit number of connections

**Black List Service**

This service validates new question against forbidden words. It loads dictionary from file `blacklist.txt` at startup. 
It's implementation is based on Hash Set for fast look-up time. 
It splits incoming sentence into words by all non-alphabetic symbols. Then each word is normalized and is processed against dictionary
If any word has been found in dictionary - this question is being rejected.


**Country Code Resolution Service**

This is service for resolving country code by IP. It uses http://freegeoip.net resource and receives data in JSON format.
IP address is taken from `HttpServletRequest` object. 
Note: if server is started and queried on local machine, IP address will be `0:0:0:0:0:0:0:1`

### Monitoring
**In-Memory DB** has its own web console which is located at http://localhost:8080/h2-console . Its connection url is defined in properties

**Server** It is also possible to monitor server health using Spring Boot Actuator: http://www.baeldung.com/spring-boot-actuators

